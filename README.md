# Introduction

This repository contains documentation that spans Evernym products. You can find additional documentation for each product in the `/docs/` folder of the source code repository for that product.

At Evernym, we believe that transparent communication will improve how we work and increase the confidence of those who rely on us. We invite you to provide feedback via issues on this project. If you are an Evernym customer or partner, you can also raise support tickets for assistance.

# Contents
* [Customer Manual](Customer-Manual/)
* [Guidance for Collaborators](Collaboration/)


# Product Documentation
* [Verity](https://gitlab.com/evernym/verity/verity-sdk/)
* [Evernym Mobile SDK](https://gitlab.com/evernym/mobile/mobile-sdk/)
