# Guidance for Collaborators

## Licensing
As part of our commitment to open innovation, the source code for Evernym products will be publicly available by the time those products are available to our general customer base. We may make limited exceptions in the following circumstances:
* When selling integrations with proprietary services (where users have already chosen not to value software freedom);
* Code tied closely to our hosting infrastructure (where it is unlikely to be useful outside of our environment and is likely to contain security-sensitive information);
* Code to enforce our commercial licenses or authorize access to our hosted services; and,
* Fixes to old versions of our products, though interested parties could backport fixes from newer releases.

The source code will either be available under [the Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0.html) or [the Evernym Business Source License](EvBSL.txt).

The [Business Source License](https://perens.com/2017/02/14/bsl-1-1/) was created to help companies commercialize open products while also guaranteeing users that the software will become fully open source within a reasonable time frame.

The Business Source License guarantees:
* The source code will be public;
* The software is available for non-production uses without a commercial license;
* The software will be available for many production uses without a commercial license, so long as they comply with any stated restrictions; and,
* After a period of time, the software converts to a traditional open source license and is no longer encumbered by any commercial restrictions.

By using our software under the Business Source License, anyone can evaluate our products at no cost and explore how they work for their use case. Customers, partners, and community users can also help the products meet their needs by contributing to the development. And most importantly, all users benefit from knowing that they are in control of their verifiable credential technologies and can either embark on a commercial relationship with Evernym or use an older version of the product that is completely free and open source.

We want Evernym’s commercial products to be available at no cost for research, evaluation, development, internal use by small organizations, and providing unpaid credentials to the public. As such, our license only restricts production use cases that are:

* beyond 500 monthly active users, or
* for revenue-generating commercial activity.

A commercial license would still be required for those looking to monetize credentials or scale their production use case. After three years from when a specific commit of code is made publicly available, these restrictions will be removed and that code will be available under the Apache 2.0 license.

[Learn more about our stance on open source software in this blog post](https://www.evernym.com/blog/evernym-commits-to-open-source/).


# Contributing

In order to ensure a welcoming and productive environment, all participants in Evernym sponsored communities are expected to follow the guidelines in the [Evernym Code of Conduct](Code-of-Conduct.md).

Most Evernym projects are hosted at [GitLab.com](http://gitlab.com/evernym). You can contribute by creating a free account on that service.

When providing a tangible contribution, contributors must agree to the [Evernym Contributor License Agreement](EvCLA.txt). This gives Evernym the legal protection necessary to maintain our business. Accepting the CLA is usually an automated process. The first time you create a merge request for an Evernym project, our bot will add a comment requesting that accept the CLA. By resolving the comment, your acceptance will be recorded.

If your merge request has multiple contributors, the various authors will need to individually add comments stating that they accept the contributor license agreement. The merge request can not be reviewed until all authors have indicated acceptance.


# Code Reviews

Each merge request will be reviewed before being accepted.

TODO: HL code review instructions
TODO: ensure CLA has been accepted
